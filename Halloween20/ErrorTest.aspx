﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ErrorTest.aspx.cs" Inherits="ErrorTest" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Chapter 20: Halloween Store</title>
    <link href="Styles/Main.css" rel="stylesheet" />
</head>
<body>
    <header><img src="Images/banner.jpg" alt="Halloween Store" /></header>
    <section>
        <form id="form1" runat="server">
            <h1>Select One:</h1>
            <p>
                <asp:Button ID="btnGenerateException" CssClass="button" runat="server" Text="Generate Exception" OnClick="btnGenerateException_Click" />
                <asp:Button ID="btnBrokenLink" CssClass="button" runat="server" Text="Broken Link" OnClick="btnBrokenLink_Click" />
            </p>
        </form>
    </section>
</body>
</html>
