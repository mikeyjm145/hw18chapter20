﻿using System;
using System.Web.UI;

/// <summary>
/// Code behind for Error Test page
/// </summary>
/// <author>
/// Michael Morguarge
/// </author>
/// <version>
/// 1.0
/// </version>
public partial class ErrorTest : Page
{
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    /// <summary>
    /// Handles the Click event of the btnGenerateException control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnGenerateException_Click(object sender, EventArgs e)
    {
        try
        {
            throw new Exception("This is a planned Exception");
        } catch (Exception x)
        {
            Session["Error"] = x;
            Response.Redirect("Error.aspx");
        }
    }

    /// <summary>
    /// Handles the Click event of the btnBrokenLink control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnBrokenLink_Click(object sender, EventArgs e)
    {
        Response.Redirect("UnknownPage.aspx");
    }
}